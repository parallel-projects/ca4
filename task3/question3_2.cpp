#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <omp.h>

double timeGetTime()
{
    struct timeval time;
    struct timezone zone;
    gettimeofday(&time, &zone);
    return time.tv_sec + time.tv_usec*1e-6;
}


const long int VERYBIG = 100000;

int main( void )
{
  int i;
  long int j, k, sum;
  double sumx, sumy, total, z;
  double starttime, elapsedtime, averagetime = 0.0;
  double thread_starttime, thread_elapsedtime;
  // ---------------------------------------------------------------------
  // Output a start message
  printf( "OpenMP Parallel Timings for %ld iterations \n", VERYBIG );
  printf("Schedule: dynamic 2000 \n\n");

  // repeat experiment several times
  for( i=0; i<6; i++ )
  {
    // get starting time
    starttime = timeGetTime();
    // reset check sum and total
    sum = 0;
    total = 0.0;
    
    // Work loop, do some work by looping VERYBIG times
    #pragma omp parallel     \
    num_threads (4) \
    private( sumx, sumy, k, thread_starttime, thread_elapsedtime )   \
    reduction( +: sum, total )
    {
      thread_starttime = timeGetTime();

      #pragma omp for schedule( dynamic, 2000) nowait
        for( int j=0; j<VERYBIG; j++ )
        {
          // increment check sum
          sum += 1;
        
          // Calculate first arithmetic series
          sumx = 0.0;
          for( k=0; k<j; k++ )
          sumx = sumx + (double)k;

          // Calculate second arithmetic series
          sumy = 0.0;
          for( k=j; k>0; k-- )
          sumy = sumy + (double)k;

          if( sumx > 0.0 )total = total + 1.0 / sqrt( sumx );
          if( sumy > 0.0 )total = total + 1.0 / sqrt( sumy );
        }
      thread_elapsedtime = timeGetTime() - thread_starttime;
      printf("T%d executation time is %10d mSecs\n", omp_get_thread_num(),
                    (int)(thread_elapsedtime * 1000));
    }
    
    // get ending time and use it to determine elapsed time
    elapsedtime = timeGetTime() - starttime;
    averagetime += elapsedtime;
      
    // report elapsed time
    printf("Time Elapsed %10d mSecs Total=%lf Check Sum = %ld\n",
                   (int)(elapsedtime * 1000), total, sum );
  }

  averagetime = averagetime / 6;
  printf("\nAverage Time Elapsed in 6 runs %10d mSec\ns", (int)(elapsedtime * 1000));

  // return integer as required by function header
  return 0;
}
// **********************************************************************
