#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "omp.h"
#include <sys/time.h>
#define     VECTOR_SIZE     1048576

  
void quickSort(float arr[], int low, int high)
{
    if (low < high)
    {
        float pivot = arr[high];
        int i = (low - 1);
        float temp;
        for (int j = low; j <= high- 1; j++)
        {
            if (arr[j] < pivot)
            {
                i++;
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        temp = arr[i+1];
        arr[i+1] = arr[high];
        arr[high] = temp;

        int p = i+1;
        quickSort(arr, low, p - 1);
        quickSort(arr, p + 1, high);
    }
}



void parallel_quickSort(float arr[], int low, int high)
{
    if (low < high)
    {
        float pivot = arr[high];
        int i = (low - 1);
        float temp;
        for (int j = low; j <= high- 1; j++)
        {
            if (arr[j] < pivot)
            {
                i++;
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        temp = arr[i+1];
        arr[i+1] = arr[high];
        arr[high] = temp;

        int pi = i+1;
        #pragma omp task firstprivate(arr,low,pi)
        {
        parallel_quickSort(arr, low, pi - 1);
        }
        #pragma omp task firstprivate(arr,low,pi)
        {
        parallel_quickSort(arr, pi + 1, high);
        }
    }
}



int main (void)
{
    printf("Student IDs:\n810196518\n810196562\n\n");

    float *floatVec;
    float *floatVecCopy;
    floatVec = new float [VECTOR_SIZE];
    floatVecCopy = new float [VECTOR_SIZE];
    srand((unsigned int)time(NULL));
    float cur;
    for (int i = 0; i < VECTOR_SIZE; i++){
        cur = float(rand())/float((RAND_MAX)) * 100.0;
        floatVec[i] = cur;
        floatVecCopy[i] = cur;
    }

    double start, end, serial_time, openMP_time;
    // serial implementation
    
    
    start = omp_get_wtime();
    quickSort(floatVec, 0, VECTOR_SIZE-1);
    end = omp_get_wtime();
    serial_time = end-start;
    printf("Serial result:\n");
    printf("serial executation time is %f s\n\n", serial_time);
    

    //parallel implementation
    start = omp_get_wtime();
    #pragma omp parallel num_threads(4) shared(floatVecCopy)
    {
        #pragma omp single nowait
        {
            parallel_quickSort(floatVecCopy, 0, VECTOR_SIZE-1);
        }
    }
    end = omp_get_wtime();
    openMP_time = end-start;
    printf("Parallel result:\n");
    printf("Parallel executation time is %f s\n\n", openMP_time);

    printf ("Speedup = %f\n\n", (float) (serial_time)/(float) openMP_time);

    
    bool same = true;
    for (int i =0; i<VECTOR_SIZE;i++){
        if (floatVec[i]!=floatVecCopy[i])
            same=false;
    }
    printf("serial ootput and parallel output are %s as each other!\n", same==true ? "same" : "not same");
    return 0;


}
