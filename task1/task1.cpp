#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "omp.h"
#include <sys/time.h>
#define     VECTOR_SIZE     1048576

int main (void)
{
    printf("Student IDs:\n810196518\n810196562\n\n");

    float *floatVec;
    floatVec = new float [VECTOR_SIZE];
    srand((unsigned int)time(NULL));
    
    for (int i = 0; i < VECTOR_SIZE; i++)
        floatVec[i] = float(rand())/float((RAND_MAX)) * 100.0;
    
    double start, end, serial_time, openMP_time;
    
    int i;
    int index, local_index;
    float max, local_max;
    float current_element;
    
    // Serial implementation
    start = omp_get_wtime();
    
    index = 0;
    max = 0.0; 
    
    for (i = 0; i < VECTOR_SIZE; i++){
        current_element = floatVec[i];
        if (current_element >= max){
            max = current_element;
            index = i;
        }
    }
    
    end = omp_get_wtime();
    serial_time = end - start;
    printf("Serial result:\n");
    printf("max number is: %f at %dth index\n", max, index);
    printf("serial executation time is %f s\n\n", serial_time);

    // OpenMP implementation
    start = omp_get_wtime();

    index = 0;
    max = 0.0;

    #pragma omp parallel num_threads(4) \
    shared(max, index, floatVec) \
    private(i, current_element, local_max, local_index)
    {        
        local_index = 0;
        local_max = 0.0; 
        
        #pragma omp for nowait
            for (i = 0; i < VECTOR_SIZE; i++){
                current_element = floatVec[i];
                if (current_element >= local_max)
                {
                    local_max = current_element;
                    local_index = i;
                }
            }
        #pragma omp critical
        {
            if (local_max >= max){
                max = local_max;
                index = local_index;
            }
        }

    }

    end = omp_get_wtime();
    openMP_time = end - start;
    printf("OpenMP result:\n");
    printf("max number is: %f at %dth index\n", max, index);
    printf("openMP executation time is %f s\n\n", openMP_time);
    printf ("Speedup (OpenMP vs Serial)= %4.2f\n", (double) serial_time / (double) openMP_time);

}
